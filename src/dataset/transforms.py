import torchvision

train_transform = torchvision.transforms.Compose([
                                                torchvision.transforms.ToPILImage(),
                                                torchvision.transforms.Resize((32, 256)),
                                                torchvision.transforms.RandomPerspective(distortion_scale=0.1, p=0.25),
                                                torchvision.transforms.ToTensor(),
                                                torchvision.transforms.Normalize([0.5], [0.5]),
                                                torchvision.transforms.RandomErasing(scale=(0.02, 0.03), ratio=(0.05, 0.1)),
                                                ])

val_transform = torchvision.transforms.Compose([
                                                torchvision.transforms.ToPILImage(),
                                                torchvision.transforms.Resize((32, 256)),
                                                torchvision.transforms.ToTensor(),
                                                torchvision.transforms.Normalize([0.5], [0.5]),
                                                ])