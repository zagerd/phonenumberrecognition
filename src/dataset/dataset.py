import cv2
import torch
import torchvision
import numpy as np
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from torch.utils.data import Dataset

ROOT = Path(__file__).resolve().parents[0].parents[0]

class PhoneNumbersDataset(Dataset):

    def __init__(
            self,
            mnist_path=ROOT / "data/",
            annotation_path=ROOT / 'data/annotations/train.txt',
            transform=None,
            is_train=True):

        self.mnist_path = mnist_path
        self.annotation = annotation_path
        self.transform = transform
        self.is_train = is_train

        self.anno = self._load_annotation()
        self.mnist = self._load_mnist()

    def __len__(self):
        return len(self.anno)

    def _load_annotation(self):
        return pd.read_csv(self.annotation, sep='\t', header=None, names=['target', 'sample'])

    def _load_mnist(self):
        dataset = torchvision.datasets.MNIST(
            self.mnist_path,
            download=True,
            transform=torchvision.transforms.Compose([
                torchvision.transforms.Lambda(lambda x: np.array(x))
            ]),
            train=self.is_train)

        return dataset

    def roi(self, symb, pads=(1, 2), tresh=50):
        y_min, y_max = np.argwhere(symb.max(axis=1) > tresh)[
            [0, -1]].reshape(-1) + np.multiply([-1, 1], pads)
        x_min, x_max = np.argwhere(symb.max(axis=0) > tresh)[
            [0, -1]].reshape(-1) + np.multiply([-1, 1], pads)

        y_min = 0 if y_min < 0 else y_min
        x_min = 0 if x_min < 0 else x_min

        return symb[y_min:y_max, x_min:x_max]

    def _create_number(self, number):
        number_background = np.zeros((32, 768), np.uint8)
        x_coor = 0
        y_start_coor = 0

        # Add padding to the start of number
        sample = np.zeros((1, np.random.randint(0, 32)), np.uint8)
        number_background[0:0+sample.shape[0],
                          x_coor:x_coor+sample.shape[1]] = sample
        x_coor += sample.shape[1]

        is_roi = np.random.choice([0, 1, 2], p=[0.45, 0.1, 0.45])
        for symb in number:

            if symb != ' ':
                class_id = int(symb)
                rand_idx = np.random.choice(torch.where(
                    self.mnist.targets == class_id)[0])

                if is_roi == 0:
                    if np.random.choice([0, 1]):
                        sample = cv2.resize(self.mnist[rand_idx][0], (np.random.choice(
                            [14, 16, 18, 20, 22, 24, 26]), ) * 2)
                        sample = self.roi(
                            sample, pads=np.random.randint(0, 2, 2), tresh=1)
                    else:
                        sample = self.mnist[rand_idx][0]
                        sample = self.roi(sample, pads=np.random.randint(
                            0, 2, 2), tresh=np.random.choice([50, 150, 200]))

                elif is_roi == 1:
                    sample = self.mnist[rand_idx][0]
                else:
                    if np.random.choice([0, 1]):
                        if np.random.choice([0, 1]):
                            sample = cv2.resize(self.mnist[rand_idx][0], (np.random.choice(
                                [14, 16, 18, 20, 22, 24, 26]), ) * 2)
                            sample = self.roi(
                                sample, pads=np.random.randint(0, 2, 2), tresh=1)
                        else:
                            sample = self.mnist[rand_idx][0]
                            sample = self.roi(sample, pads=np.random.randint(
                                0, 2, 2), tresh=np.random.choice([50, 150, 200]))
                    else:
                        sample = self.mnist[rand_idx][0]

            else:
                sample = np.zeros((1, np.random.randint(0, 32)), np.uint8)

            y_coor = np.random.randint(
                y_start_coor, number_background.shape[0] - sample.shape[0])

            number_background[y_coor:y_coor+sample.shape[0],
                              x_coor:x_coor+sample.shape[1]] = sample
            x_coor += sample.shape[1]

        # Add padding to the end of number
        sample = np.zeros((1, np.random.randint(0, 32)), np.uint8)
        number_background[0:0+sample.shape[0],
                          x_coor:x_coor+sample.shape[1]] = sample
        x_coor += sample.shape[1]

        if np.random.choice([0, 1]):
            number_background = np.clip(
                number_background, 0, np.random.randint(100, 250))

        number_background = number_background[:, :x_coor+1]

        return number_background

    def __getitem__(self, idx):
        target, sample = self.anno.iloc[idx]
        target = np.array([int(d) for d in str(target)])

        number = self._create_number(sample)

        if self.transform is not None:
            image = self.transform(number)
        return image, target


if __name__ == '__main__':

    from tqdm import tqdm
    import matplotlib.pyplot as plt
    import os

    transform = torchvision.transforms.Compose([
        torchvision.transforms.ToPILImage(),
        torchvision.transforms.Resize((32, 256)),
        torchvision.transforms.RandomPerspective(distortion_scale=0.1, p=0.25),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize([0.5], [0.5]),
        torchvision.transforms.RandomErasing(
            scale=(0.02, 0.03), ratio=(0.05, 0.1)),
    ])

    dataset = PhoneNumbersDataset(transform=transform)

    def transform_img(img):
        img = img.numpy()
        img = img.transpose((1, 2, 0))
        img = (0.5) * img + (0.5)
        img = np.clip(img, 0, 1)
        return img

    out_dir = ROOT / 'data/test_dataset_numbers'
    for idx, (img, target) in tqdm(enumerate(dataset)):
        img = transform_img(img)
        out_fpath = os.path.join(out_dir, str(idx) + '.jpg')

        plt.imshow(img, cmap='gray')
        plt.title(target)
        plt.savefig(out_fpath)

        if idx >= 20:
            break

    print('Done!')
