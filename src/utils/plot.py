import numpy as np
import cv2


def transform_img(img, STD=[0.5], MEAN=[0.5]):
    img = img.transpose((0, 2, 3, 1))
    img = STD * img + MEAN
    img = np.clip(img, 0, 1)
    return img


def plot(img, targets, preds, n_images):
    img = transform_img(img).transpose((0, 3, 1, 2)).astype(np.float32)[:n_images] #  First n images from batch
    img = np.stack((img,)*3, axis=1).squeeze(2)
    text_img = (np.ones((n_images, 64, 256, 3))).astype(np.float32)
    for i, t_img in enumerate(text_img):
        
        if np.all(targets[i] == preds[i], axis=0):
            color = (0, 1, 0)
        else:
            color = (1, 0, 0)

        cv2.putText(t_img, f'TRUE: {str(targets[i])}', (0, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2, cv2.LINE_AA, False)
        cv2.putText(t_img, f'PRED: {str(preds[i])}', (0, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2, cv2.LINE_AA, False)

    return np.concatenate((img, text_img.transpose((0, 3, 1, 2))), axis=2)
    
    # BS H W C ---> BS C H W
    #  0 1 2 3 --->  0 3 1 2