import numpy as np


class StrNumberCreater:
    
    def create_random_number(self):
        return  '8' + ''.join(np.random.randint(0, 10, 10).astype('str'))
    
    def modify_number(self, number):
        
        # Possible options of phone number
        options = {'country_options': ['8'], 'city_options': [' ', ''], 'sep': ['', ' ']}

        # Code contry start with +7 or 8
        # country_code = np.random.choice(options['country_options'])
        country_code = '+7' if number[0] == '7' else '8'

        # Code city with brackets or not
        city_sep = np.random.choice(options['sep'])
        city_code = city_sep.join(self.slicer(number[1:4])[::-1])

        # Subscriber number
        sub_sep = np.random.choice(options['sep'])
        sub_number = sub_sep.join(self.slicer(number[-7:])[::-1])

        # Choice separator
        sep = np.random.choice(options['sep'])
        
        return sep.join([country_code, city_code, sub_number])
    
    # Subscriber number slicing
    def slicer(self, numbers_list):
        if len(numbers_list) == 0:
            return []
        else:
            sep = np.random.randint(1, len(numbers_list) + 1)
            return self.slicer(numbers_list[sep:]) + [numbers_list[:sep]]        
        

if __name__ == '__main__':

    import time

    SNC = StrNumberCreater()

    # Creating unique train data
    train_random_numbers = []
    train_modified_numbers = []
    for i in range(120000):
        random_number = SNC.create_random_number()
        if random_number not in train_random_numbers:
            train_random_numbers.append(random_number)
            train_modified_numbers.append(SNC.modify_number(random_number))

    # Creating unique validation data
    val_random_numbers = []
    val_modified_numbers = []
    for i in range(10000):
        random_number = SNC.create_random_number()
        if (random_number not in val_random_numbers) and (random_number not in train_random_numbers):
            val_random_numbers.append(random_number)
            val_modified_numbers.append(SNC.modify_number(random_number))
        
    train = zip(train_random_numbers, train_modified_numbers)
    val = zip(val_random_numbers, val_modified_numbers)

    TRAIN_ANNO_PATH = "/mnt/cephfs/projects/Zakharov_examples/PhoneNumberRecognition/data/annotations/train_only_digits.txt"
    VAL_ANNO_PATH = "/mnt/cephfs/projects/Zakharov_examples/PhoneNumberRecognition/data/annotations/val_only_digits.txt"


    with open(TRAIN_ANNO_PATH, 'w') as t, open(VAL_ANNO_PATH, 'w') as v:
    
        # Creating train .txt file
        for rn, mn in train:
            t.write(f'{rn}\t{mn}\n')
        t.close()
        
        # Creating validation .txt file
        for rn, mn in val:
            v.write(f'{rn}\t{mn}\n')
        v.close()
    
    print("DONE!")