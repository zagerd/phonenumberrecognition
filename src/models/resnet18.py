import torch, torchvision
from torch import nn


class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()

        self.base_model = torchvision.models.resnet18(pretrained=True)
        self.base_model.conv1 = nn.Conv2d(1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        self.fc_cls = nn.Linear(self.base_model.fc.in_features, 10*11)
        self.base_model.fc = nn.Identity()

    def forward(self, x):
        out = self.base_model(x)
        out = self.fc_cls(out) 
        out = out.reshape(-1, 10, 11)
        return out

# print(Model()(torch.rand(1, 1, 200, 200)))
# print(Model())
