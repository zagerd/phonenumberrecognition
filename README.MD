# Recognition of handwritten phone numbers

**This project shows how you can train a model to recognize phone numbers starting with "8" (Russian) and run it on a web page.**

## Getting started

1. Create new environment and activate it:
```
$ conda create --name <env_name> python=3.8
$ conda activate <env_name>
```
2. Download and install all required packages:
```
$ conda install --file requirements.txt
```
3. Run Tensorboard to track results:
```
$ cd <project_name>
$ tensorboard --logdir runs\train
```
4. Run train.py:
```
$ python train.py
```

## Some examples of data

The data was artificially generated using the MNIST dataset.

<img src="./docs/images/image_2022-03-07_14-14-17.png" width="50%" />
<img src="./docs/images/image_2022-03-07_14-15-12.png" width="50%" />
<img src="./docs/images/image_2022-03-07_14-26-47.png" width="50%" />

## TensorBoard

In TensorBoard, we can track loss, accuracy, and draw correct and incorrect model predictions.

<img src="./docs/images/image_2022-03-07_14-19-10.png" width="50%" />
<img src="./docs/images/image_2022-03-07_14-19-24.png" width="50%" />

## Inference

1. After training the model, it should be converted to a .jit format using **src\utils\save_jit.py**.
2. To make it possible to show the performance of the model, a web form was written using HTML/CSS/JS. And it's all running with Flask (look at folder **flask**).
3. Docker allows us to build our project and run it on another machine. For this we use **dockerfile**

<img src="./docs/images/chrome_CysvmegCq0.gif" width="100%" />
