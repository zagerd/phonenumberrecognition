from flask import Flask, render_template, request
from pathlib import Path
import sys

ROOT = Path(__file__).resolve().parent.parent
sys.path.append(str(ROOT)) if str(ROOT) not in sys.path else None

from inference import *
import os
import base64
import re

SAVE_PATH = './data/test_data_from_web'
app = Flask(__name__)

@app.route("/")
def home():
    return render_template("paint.html")


@app.route('/hook', methods=['GET', 'POST'])
def recognize_image():
    image_b64 = request.values['imageBase64']
    image_data = re.sub('^data:image/.+;base64,', '', image_b64)
    image_path = f"{SAVE_PATH}/flask_{len(os.listdir(SAVE_PATH))}.jpg"
    with open(image_path, "wb") as fh:
        fh.write(base64.decodebytes(bytes(image_data, encoding='UTF-8')))
    
    recognized_number = inference(image_path, base_model)
    return recognized_number

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8080', debug=True)
