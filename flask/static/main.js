var colour = $(".selected").css("background-color");
var $canvas = $("canvas");
var context = $canvas[0].getContext("2d");
var lastEvent;
var mouseDown = false;

context.fillStyle = "white";
context.fillRect(0, 0, $canvas[0].width, $canvas[0].height);

// On mouse events on the canvas
$canvas.mousedown(function (e) {
    lastEvent = e;
    mouseDown = true;
}).mousemove(function (e) {
    // Draw lines
    if (mouseDown) {
        context.beginPath();
        context.moveTo(lastEvent.offsetX, lastEvent.offsetY);
        context.lineTo(e.offsetX, e.offsetY);
        context.strokeStyle = colour;
        context.lineWidth = 10;
        context.lineCap = 'round';
        context.stroke();
        lastEvent = e;
    }
}).mouseup(function () {
    mouseDown = false;
}).mouseleave(function () {
    $canvas.mouseup();
});

// Clear the canvas when button is clicked
function clear_canvas() {
    context.fillStyle = "white";
    context.fillRect(0, 0, $canvas[0].width, $canvas[0].height);
}

const labelElement = document.getElementById('label1');

function recognize_number() {
    var imgURL = $canvas[0].toDataURL('image/jpg');
    $.ajax({
        type: "POST",
        url: "/hook",
        data:{
          imageBase64: imgURL
        }
      }).done(function(output) {
        labelElement.innerText = output;
        console.log('sent');
      });
}
