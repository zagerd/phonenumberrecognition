import cv2
import PIL
import torch
import argparse
import torchvision
from pathlib import Path


ROOT = Path(__file__).resolve().parents[0]
JIT_PATH = ROOT / 'jit/ResNet18_PhoneNumberRecognition.jit'


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--jit_path', type=str,
                        default=JIT_PATH, help='.jit file path')

    args = parser.parse_args()
    return args


def inference(image_path, model):

    test_number = cv2.imread(image_path)
    test_number = cv2.cvtColor(test_number, cv2.COLOR_BGR2GRAY)

    val_transform = torchvision.transforms.Compose([
        torchvision.transforms.ToPILImage(),
        PIL.ImageOps.invert,
        torchvision.transforms.Resize((32, 256)),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize([0.5], [0.5]),
    ])

    test_number = val_transform(test_number)

    with torch.no_grad():
        logits = model(test_number.unsqueeze(0))
        preds = torch.argmax(torch.softmax(logits, dim=1), axis=1)

    return ''.join(preds.numpy().astype('str')[0])


args = parse_args()

base_model = torch.jit.load(args.jit_path)

if __name__ == '__main__':

    args = parse_args()

    base_model = torch.jit.load(args.jit_path)

    pred = inference(ROOT / 'data/test_inference_data/test.jpg', base_model)
    print(pred)
