# select the base image available in the platform
FROM python:3.8

COPY ./ /app/

RUN pip install --no-cache-dir -r /app/requirements.txt
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y

# default working directory
WORKDIR /app/

# the entry point to launch the application
ENTRYPOINT python /app/flask/server.py
